# WinRM-Shell [![Ruby2.3-shield]](https://www.ruby-lang.org/en/news/2015/12/25/ruby-2-3-0-released/) [![License-shield]](LICENSE)
WinRM shell

## Description & Purpose
This is an improved WinRM shell based on [Alamot](https://github.com/Alamot/code-snippets/tree/master/winrm)'s original.

## Features
 - Command history
 - WinRM command completion
 - Local file completion
 - Upload and download
 - List services
 - Run PowerShell FullLanguage mode
 - Execute PowerShell scripts
 - In memory DLLs
 - In memory C# compiled executables
 - Colorization

## Requirements
Ruby 2.3 or higher and Ruby Gems: `winrm`, `winrm-fs`, `colorize` and `stringio`.

`sudo gem install winrm winrm-fs colorize stringio`

## Quick Start
 - Edit the connection parameters.
 - Run it: `ruby evil-winrm.rb`

Settings:
```
# Optionally set default path for scripts and executables:
$scripts = "/root/scripts/"
$executables = "/root/tools/"

# Update endpoint, user, and password to the target:
conn = WinRM::Connection.new(
    endpoint: 'http://10.0.0.1:5985/wsman',
    user: 'contoso\Administrator',
    password: 'Pass0wrd!',
    :no_ssl_peer_verification => true,
    # transport: :ssl,
    # client_cert: 'certnew.cer',
    # client_key: 'client.key',
)
```

Based on [Alamot]'s original WinRM shell.

Thanks to [3v4Si0N] for the DLL loader.

<!-- Github URLs -->
[Alamot]: https://github.com/Alamot
[3v4Si0N]: https://github.com/3v4Si0N/

<!-- Badges URLs -->
[Ruby2.3-shield]: https://img.shields.io/badge/ruby-2.3%2B-blue.svg?style=flat-square&colorA=273133&colorB=ff0000 "Ruby 2.3 or later"
[License-shield]: https://img.shields.io/badge/license-LGPL%20v3%2B-blue.svg?style=flat-square&colorA=273133&colorB=bd0000 "LGPL v3+"